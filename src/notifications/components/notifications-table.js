import React from "react";
import Table from "../../commons/tables/table";

class NotificationsTable extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            tableData: props.tableData,
        }
    }

    columns = [
        {
            text: 'ID',
            dataField: 'id',
            editable: false
        },
        {
            text: 'Notification',
            dataField: 'notification',
        },
        {
            text: 'patient ID',
            dataField: 'patientID',
        }]

    render() {
        return(
            <div>
                <Table data={this.state.tableData} columns={this.columns}/>
            </div>
        )
    }
}

export default NotificationsTable;