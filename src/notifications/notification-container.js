import React from "react";
import NotificationsTable from "./components/notifications-table.js";
import * as API_USERS from "./api/notifications-api.js"

class NotificationContainer extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
        }
    }

    componentDidMount() {
        this.fetchNotifications();
    }

    fetchNotifications(){
        return API_USERS.getNotifications((result, status, err) =>{
            if(result !== null && status === 200){
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            }else{
                this.setState({
                    errorStatus: status,
                    error: err,
                });
            }
        });
    }

    render() {
        return (
            <div>
                {this.state.isLoaded && <NotificationsTable tableData={this.state.tableData}/>}
            </div>
        );
    }
}

export default NotificationContainer;
