import React, {Component} from "react";
import BootstrapTable from 'react-bootstrap-table-next';
import cellEditFactory from 'react-bootstrap-table2-editor';

class Table extends Component{
    constructor(props) {
        super(props);

        this.state = {
            data: props.data,
            columns: props.columns,
        };
    }

    render() {
        return(
            <div>
                <BootstrapTable keyField="id" data={this.state.data} columns={this.state.columns} cellEdit={cellEditFactory({mode: 'click', blurToSave: true})}/>
            </div>

        )
    }
}

export default Table;
