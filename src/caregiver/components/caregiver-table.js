import React from "react";
import Table from "../../commons/tables/table";
import * as API_USERS from "../api/caregiver-api";

function handleDeleteCareGiver(item) {
    return API_USERS.deleteCareGiver(item.id, (result, status, error) => {
        if (result !== null && (status === 200 || status === 201)) {
            console.log("Successfully deleted caregiver with id: " + result);
            //this.reloadHandler();
        } else {
            console.log(error);
        }
    });
}

function handleUpdateCareGiver(careGiver) {
    return API_USERS.updateCareGiver(careGiver, (result, status, error) => {
        if(result !== null && (status === 200 || status === 201)){
            console.log("Successfully updated person with id:" + result);
        }else{
            console.log(error)
        }
    })
}

function executeUpdate(rowDetails) {

    let caregiver = {
        id: rowDetails.id,
        name: rowDetails.name,
        birthDate: rowDetails.birthDate,
        gender: rowDetails.gender,
        address: rowDetails.address,
    };
    console.log(caregiver)
    handleUpdateCareGiver(caregiver)
}

const columns = [
    {
        text: 'ID',
        dataField: 'id',
    },
    {
        text: 'Name',
        dataField: 'name',
    },
    {
        text: 'Birth Day',
        dataField: 'birthDate',
    },
    {
        text: 'Address',
        dataField: 'address',
    },
    {
        text: 'Gender',
        dataField: 'gender',
    },
    {
        
        text: 'Actions',
        dataField: '',
        formatter: (rowContent, row) => {
            return(
                <div>
                    <button onClick={executeUpdate.bind(this, row)}>Update</button>
                    <button onClick={handleDeleteCareGiver.bind(this, row)}>Delete</button>
                </div>
            )
        },
        editable: false,
    }]

class CaregiverTable extends React.Component{
    constructor(props) {
        super(props);
        console.log(props.tableData, "CaregiverTable");
        this.state = {
            tableData: props.tableData,
        };
    }

    render() {
        return(
            <div>
                <Table data={this.state.tableData} columns={columns}/>
            </div>

        )

    }
}

export default CaregiverTable;