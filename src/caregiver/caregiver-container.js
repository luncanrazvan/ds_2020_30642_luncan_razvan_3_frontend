import React from "react";
import CaregiverTable from "./components/caregiver-table";
import * as API_USERS from "./api/caregiver-api"
import {Button, Modal, ModalBody, ModalHeader} from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import CaregiverForm from "./components/caregiver-form";

class CaregiverContainer extends React.Component{

    constructor(props) {
        super(props);
        this.reload = this.reload.bind(this);
        this.toggleForm = this.toggleForm.bind(this);
        this.redirectToNotifications = this.redirectToNotifications.bind(this);
        this.state = {
            selected: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
        }
    }

    componentDidMount() {
        this.fetchCareGivers();
        //localStorage.setItem('caregiver', '');
    }


    fetchCareGivers(){
        return API_USERS.getCareGivers((result, status, err) =>{
            if(result !== null && status === 200){
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            }else{
                this.setState({
                    errorStatus: status,
                    error: err,
                });
            }
        });
    }

    toggleForm(){
        this.setState({selected: !this.state.selected});
    }

    redirectToNotifications(){
        window.location.href='/notifications'
    }

    reload(){
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchCareGivers();
    }

    render() {
        return (
            <div>
                <Button onClick={this.toggleForm}>Add Caregiver</Button>&nbsp;
                <Button onClick={this.redirectToNotifications}>Notifications</Button>
                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Caregiver: </ModalHeader>
                    <ModalBody>
                        <CaregiverForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>
                {this.state.isLoaded && <CaregiverTable tableData={this.state.tableData}/>}
            </div>
        );
    }
}

export default CaregiverContainer;