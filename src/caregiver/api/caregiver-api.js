import {HOST} from '../../commons/hosts'
import RestApiClient from "../../commons/api/rest-client"

const endpoint = {
    caregiver: '/caregiver'
}

function getCareGivers(callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver, {
        method:'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postCareGiver(caregiver, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver,{
        method: 'POST',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(caregiver)
    });
    RestApiClient.performRequest(request, callback);
}

function updateCareGiver(caregiver, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver,{
        method: 'PUT',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(caregiver)
    });
    RestApiClient.performRequest(request, callback);
    window.location.reload(false);
}

function deleteCareGiver(params, callback) {

    let request = new Request(HOST.backend_api + endpoint.caregiver + '/' + params,{
        mode:"cors",
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem("Authorization")}`,
            'Accept': 'application/json',
            'Content-Type':'application/json',
            'Access-Control-Allow-Origin':'*',
        },
        body: JSON.stringify(params)

    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
    window.location.reload(false);
}

export {
    getCareGivers,
    postCareGiver,
    updateCareGiver,
    deleteCareGiver,
}