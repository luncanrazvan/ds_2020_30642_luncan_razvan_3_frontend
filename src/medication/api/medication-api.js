import {HOST} from '../../commons/hosts'
import RestApiClient from "../../commons/api/rest-client"

const endpoint = {
    medication: '/medication'
}

function getMedications(callback) {
    let request = new Request(HOST.backend_api + endpoint.medication, {
        method:'GET',
    });
    console.log(request.url)
    RestApiClient.performRequest(request, callback);
}

function postMedication(medication, callback) {
    let request = new Request(HOST.backend_api + endpoint.medication,{
        method: 'POST',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medication)
    });
    RestApiClient.performRequest(request, callback);
    window.location.reload(false);
}

function updateMedication(medication, callback) {
    let request = new Request(HOST.backend_api + endpoint.medication,{
        method: 'PUT',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medication)
    });
    RestApiClient.performRequest(request, callback);
    window.location.reload(false);
}

function deleteMedication(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.medication + '/' + params,{
        mode:"cors",
        method: 'DELETE',
        headers:{
            'Accept': 'application/json',
            'Content-Type':'application/json',
            'Access-Control-Allow-Origin':'*',
        },
        body: JSON.stringify(params)
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
    window.location.reload(false);
}

export {
    getMedications,
    postMedication,
    updateMedication,
    deleteMedication,
}