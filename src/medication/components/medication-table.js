import React from "react";
import Table from "../../commons/tables/table";
import * as API_USERS from "../api/medication-api";

function handleDeleteMedication(item) {
    return API_USERS.deleteMedication(item.id, (result, status, error) => {
        if(result !== null && (status === 200 || status === 201)){
            console.log("Successfully deleted medication with id:" + result);
        }else{
            console.log(error);
        }
    });
}

function handleUpdateMedication(medication) {
    return API_USERS.updateMedication(medication, (result, status, error) => {
        if(result !== null && (status === 200 || status === 201)){
            console.log("Successfully updated person with id:" + result);
        }else{
            console.log(error)
        }
    })
}

function executeUpdate(rowDetails) {

    let medication = {
        id: rowDetails.id,
        name: rowDetails.name,
        sideEffects: rowDetails.sideEffects,
        dosage: rowDetails.dosage,
    };
    handleUpdateMedication(medication)
}

class MedicationTable extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            tableData: props.tableData,
        }
    }

    columns = [
        {
            text: 'ID',
            dataField: 'id',
        },
        {
            text: 'Name',
            dataField: 'name',
        },
        {
            text: 'Side Effects',
            dataField: 'sideEffects',
        },
        {
            text: 'Dosage',
            dataField: 'dosage',
        },
        {

            text: 'Actions',
            dataField: '',
            formatter: (rowContent, row) =>{
                return(
                    <div>
                        <button onClick={executeUpdate.bind(this, row)}>Update</button>
                        <button onClick={handleDeleteMedication.bind(this, row)}>Delete</button>
                    </div>
                )
            },
            editable: false,
        }]

    render() {
        return(
            <div>
                <Table data = {this.state.tableData} columns={this.columns}/>
            </div>
        )
    }
}

export default MedicationTable;