import React from "react";
import MedicationTable from "./components/medication-table";
import * as API_USERS from "./api/medication-api"
import {Button, Modal, ModalBody, ModalHeader} from 'reactstrap';
import MedicationForm from "./components/medication-form";

class MedicationContainer extends React.Component{

    constructor(props) {
        super(props);
        this.reload = this.reload.bind(this);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            selected: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
        }
    }

    componentDidMount() {
        this.fetchMedications();

    }

    fetchMedications(){
        return API_USERS.getMedications((result, status, err) =>{
            if(result !== null && status === 200){
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            }else{
                this.setState({
                    errorStatus: status,
                    error: err,
                });
            }
            console.log(this.state.tableData);
        });
    }

    toggleForm(){
        this.setState({selected: !this.state.selected});
        console.log(this.state.selected)
    }

    reload(){
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchMedications();
    }

    render() {
        return (
            <div>
                <Button onClick={this.toggleForm}>Add Medication</Button>
                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Medication: </ModalHeader>
                    <ModalBody>
                        <MedicationForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>
                {this.state.isLoaded && <MedicationTable tableData={this.state.tableData}/>}
            </div>
        );
    }
}

export default MedicationContainer;
