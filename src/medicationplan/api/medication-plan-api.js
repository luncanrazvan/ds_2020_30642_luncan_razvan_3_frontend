import {HOST} from '../../commons/hosts'
import RestApiClient from "../../commons/api/rest-client"

const endpoint = {
    medicationPlan: '/medicationPlan'
}

function postMedicationPlan(medicationPlan, callback) {
    let request = new Request(HOST.backend_api + endpoint.medicationPlan,{
        method: 'POST',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medicationPlan)
    });
    RestApiClient.performRequest(request, callback);
    window.location.reload(false);
}

export {
    postMedicationPlan,
}