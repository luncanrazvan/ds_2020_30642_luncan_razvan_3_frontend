import React from "react";
import validate from "../medication/components/validators/medication-validator";
import * as API_USERS_MEDICATION from "../medication/api/medication-api";
import * as API_USERS_PATIENT from  "../patient/api/patient-api";
import {Col, FormGroup, Input, Label, Row} from "reactstrap";
import Button from "react-bootstrap/Button";
import Select from "react-select";

function handleMultipleDropDown(e){

    let medicationIDs = []

    if(e != null){

        for(let i = 0; i < e.length; i++){
            medicationIDs.push(e[i].value)
        }

        this.setState({medicationIDs: medicationIDs})
    }
}

class MedicationPlanForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,
            patientID: props.patientID,
            medicationList: [],
            medicationIDs: [],
            isLoaded: false,

            formControls: {
                intakeIntervals: {
                    value: '',
                    placeholder: 'Enter Intake Intervals...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                beginningDate: {
                    value: '',
                    placeholder: 'Enter beginning date...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 5,
                        isRequired: true
                    }
                },
                endingDate: {
                    value: '',
                    placeholder: 'Enter ending date...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                }
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    fetchMedication(){
        return API_USERS_MEDICATION.getMedications((result, status, err) =>{
            if(result !== null && status === 200){

                let auxOptions = [{value: '', label: ''}];
                for(let i = 0; i < result.length; i++){
                    auxOptions.push({value: result[i].id, label: result[i].name})
                }

                this.setState({
                    medicationList: auxOptions,
                    isLoaded: true
                });
            }else{
                this.setState({
                    errorStatus: status,
                    error: err,
                });
            }
            console.log(this.state.tableData);
        });
    }

    componentDidMount() {
        this.fetchMedication()
    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    handleSubmit() {
        if(this.state.formIsValid){

            let medicationPlanDTO = {
                patientID: this.state.patientID,
                intakeIntervals: this.state.formControls.intakeIntervals.value,
                beginningDate: this.state.formControls.beginningDate.value,
                endingDate: this.state.formControls.endingDate.value,
                medicationIDsList: this.state.medicationIDs
            }
            API_USERS_PATIENT.assignMedicationPlan(medicationPlanDTO)
        }
    }

    render() {
        return (
            <div>
                <FormGroup id='intakeIntervals'>
                    <Label for='intakeIntervalsField'> Intake Intervals: </Label>
                    <Input name='intakeIntervals' id='intakeIntervalsField'
                           placeholder={this.state.formControls.intakeIntervals.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.intakeIntervals.value}
                           touched={this.state.formControls.intakeIntervals.touched? 1 : 0}
                           valid={this.state.formControls.intakeIntervals.valid}
                           required
                    />
                    {this.state.formControls.intakeIntervals.touched && !this.state.formControls.intakeIntervals.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='beginningDate'>
                    <Label for='beginningDateField'> Beginning Date: </Label>
                    <Input name='beginningDate' id='beginningDateField' placeholder={this.state.formControls.beginningDate.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.beginningDate.value}
                           touched={this.state.formControls.beginningDate.touched? 1 : 0}
                           valid={this.state.formControls.beginningDate.valid}
                           required
                    />
                    {this.state.formControls.beginningDate.touched && !this.state.formControls.beginningDate.valid &&
                    <div className={"error-message"}> * Email must have a valid format</div>}
                </FormGroup>

                <FormGroup id='endingDate'>
                    <Label for='endingDateField'> Ending Date: </Label>
                    <Input name='endingDate' id='endingDateField' placeholder={this.state.formControls.endingDate.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.endingDate.value}
                           touched={this.state.formControls.endingDate.touched? 1 : 0}
                           valid={this.state.formControls.endingDate.valid}
                           required
                    />
                </FormGroup>

                <FormGroup>
                    <Label>Medications: </Label>
                    {this.state.isLoaded && <Select options={this.state.medicationList} isMulti={true} onChange={handleMultipleDropDown.bind(this)}/>}
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>Submit</Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default MedicationPlanForm;