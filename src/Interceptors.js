var axios = require("axios");

export const jwtToken = localStorage.getItem("Authorization");

axios.interceptors.request.use(
    function(config) {
        if (jwtToken) {
            console.log("Aici ajung?")
            config.headers["Authorization"] = "Bearer " + jwtToken;
        }
        return config;
    },
    function(err) {
        return Promise.reject(err);
    }
);