import {HOST} from '../../commons/hosts'
import RestApiClient from "../../commons/api/rest-client"

const endpoint = {
    home: '/home'
}

function getUserLogin(loginDetails, callback) {
    let request = new Request(HOST.backend_api + endpoint.home,{
        method: 'POST',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(loginDetails)
    });
    RestApiClient.performRequest(request, callback);
    window.location.reload(false);
}

export {
    getUserLogin,
}