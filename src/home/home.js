import React from 'react';
import Button from "react-bootstrap/Button";
import {BrowserRouter as Router} from "react-router-dom";
import axi from "axios"
import {HOST} from '../commons/hosts'


/*function onLogIn() {

    let loginDetails = {
        userNameLogin: this.state.userNameValue,
        passwordLogin: this.state.passwordValue,
    }

    axi.post(HOST.backend_api + '/home', loginDetails)
        .then(response => {
            if (response !== null){
                localStorage.setItem('medicationPlanPatient', 'true')
                localStorage.setItem('caregivers patients', 'true')
                window.location.href=response.data
            }
        })
}*/

function onLogIn() {

    let loginDetails = {
        userName: this.state.userNameValue,
        password: this.state.passwordValue,
    }

    axi.post(HOST.backend_api + '/authenticate', loginDetails)
        .then(res => {
            if(res !== null){
                localStorage.setItem('Authorization', res.data.jwtToken);
                localStorage.setItem('medicationPlanPatient', 'true')
                localStorage.setItem('caregivers patients', 'true')
                window.location.href=res.data.link
            }
        });
}

class Home extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            userNameValue: '',
            passwordValue: '',
            apiResponse: '',
            isLoaded:false,
        }
    }

    componentDidMount() {
        localStorage.setItem('patient', '');
        localStorage.setItem('caregiver', '');
        localStorage.setItem('medicationPlanPatient', '');
        localStorage.setItem('caregivers patients', '');
        localStorage.setItem('socket connected', '');
        localStorage.setItem('pill dispenser', '');
        localStorage.setItem('Authorization', '');
    }

    updateUserNameInputValue(evt){
        this.setState({
            userNameValue: evt.target.value
        })
    }

    updatePasswordInputValue(evt){
        this.setState({
            passwordValue: evt.target.value
        })
    }

    render() {
        return(
            <Router>
                <div style={{position: 'absolute', left: '50%', top: '50%', transform: 'translate(-50%, -50%)'}}>
                    <div>
                        <label style={{marginRight: '5px'}}>user name:</label>
                        <input value={this.state.userNameValue} onChange={evt => this.updateUserNameInputValue(evt)}/>
                    </div>
                    <div>
                        <label style={{marginRight: '11px'}}>password:</label>
                        <input value={this.state.passwordValue} onChange={evt => this.updatePasswordInputValue(evt)} type="password"/>
                    </div>
                    <Button onClick={() =>  window.location.href='/doctor'} style={{marginLeft: '90px'}}>Doctor</Button>
                    <Button onClick={onLogIn.bind(this)} style={{marginLeft: '20px'}}>Submit</Button>
                </div>
            </Router>
        )
    }
}

export default Home