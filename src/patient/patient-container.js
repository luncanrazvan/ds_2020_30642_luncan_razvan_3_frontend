import React from "react";
import PatientTable from "./components/patient-table";
import * as API_USERS from "./api/patient-api"
import * as CareGiverCRUD from "../caregiver/api/caregiver-api"
import {Button, Modal, ModalBody, ModalHeader} from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import PatientForm from "./components/patient-form";

class PatientContainer extends React.Component{

    constructor(props) {
        super(props);
        this.reload = this.reload.bind(this);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            selected: false,
            tableData: [],
            options: [{value: '', label: ''}],
            optionsMultiSelect: [{name: '', id: 0}],
            isLoaded: false,
            areOptionsLoaded:false,
            areMultiOptionsLoaded: false,
            errorStatus: 0,
            error: null,
        }
    }

    componentDidMount() {
        this.fetchPatients();
    }


    fetchPatients(){
        API_USERS.getPatients((result, status, err) =>{
            if(result !== null && status === 200){
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            }else{
                this.setState({
                    errorStatus: status,
                    error: err,
                });
            }
        });

        CareGiverCRUD.getCareGivers((result, status, err)=>{
            if(result !== null && status === 200){
                let auxOptions = [{value: '', label: ''}];
                for(let i = 0; i < result.length; i++){
                    auxOptions.push({value: result[i].id, label: result[i].name})
                }

                this.setState({
                    options: auxOptions,
                    areOptionsLoaded: true,
                });

            }else{
                this.setState({
                    errorStatus: status,
                    error: err,
                });
            }

        });
    }

    toggleForm(){
        this.setState({selected: !this.state.selected});
    }

    reload(){
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchPatients();
    }

    render() {
        return (
            <div>
                <Button onClick={this.toggleForm}>Add Patient</Button>
                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Medication: </ModalHeader>
                    <ModalBody>
                        <PatientForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>
                {this.state.isLoaded && this.state.areOptionsLoaded  && <PatientTable tableData={this.state.tableData} options={this.state.options}/>}
            </div>
        );
    }
}

export default PatientContainer;
