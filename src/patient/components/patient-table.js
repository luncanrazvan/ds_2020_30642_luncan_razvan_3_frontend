import React from "react";
import Table from "../../commons/tables/table";
import * as API_USERS from "../api/patient-api";
import Select from 'react-select'
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import MedicationPlanForm from "../../medicationplan/medication-plan-form";

function handleDeletePatients(item) {

    return API_USERS.deletePatients(item.id, (result, status, error) => {
       if (result !== null && (status === 200 || status === 201)) {
           console.log("Successfully deleted patient with id: " + result);
       } else {
           console.log(error);
       }
   });
}

function handleUpdatePatient(patient) {
    return API_USERS.updatePatient(patient, (result, status, error) => {
        if(result !== null && (status === 200 || status === 201)){
            console.log("Successfully updated person with id:" + result);
        }else{
            console.log(error)
        }
    })
}

function handleAssignCaregiver(caregiverPatient) {
    return API_USERS.assignCareGiver(caregiverPatient, (result, status, error) => {
        if(result !== null && (status === 200 || status === 201)){
            console.log("Successfully updated person with id:" + result);
        }else{
            console.log(error)
        }
    })
}

function executeUpdate(rowDetails) {

    let patient = {
        id: rowDetails.id,
        name: rowDetails.name,
        birthDate: rowDetails.birthDate,
        gender: rowDetails.gender,
        address: rowDetails.address,
        medicalRecord: rowDetails.medicalRecord,
    };

    handleUpdatePatient(patient)
}

function handleDropDown(e) {
    this.setState({careGiverID: e.value})
}

function handleAssignButton(rowDetails) {

    let careGiverPatient = {
        patientID: rowDetails.id,
        careGiverID: this.state.careGiverID,
    };

    handleAssignCaregiver(careGiverPatient)
}

class PatientTable extends React.Component{

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            selected:false,
            tableData: props.tableData,
            options: props.options,
            isLoaded: false,
            careGiverID: 0,
            medicationIDs: [],
            selectedIDFroMedicationPlan: 0,
        }
    }

    toggleForm(rowDetails){
        this.setState({selectedIDFroMedicationPlan: rowDetails.id})
        this.setState({selected: !this.state.selected});
    }


    columns = [
        {
            text: 'ID',
            dataField: 'id',
            editable: false
        },
        {
            text: 'Name',
            dataField: 'name',
        },
        {
            text: 'Birth Day',
            dataField: 'birthDate',
        },
        {
            text: 'Address',
            dataField: 'address',
        },
        {
            text: 'Gender',
            dataField: 'gender',
        },
        {
            text: 'Medical Record',
            dataField: 'medicalRecord',
        },
        {
            text: 'Care Giver',
            dataField: '',
            formatter: () => {
                return(
                    <div>
                        <Select options={this.state.options} onChange={handleDropDown.bind(this)}/>
                    </div>
                )
            },
            editable: false,
        },
        {
            text: 'Actions',
            dataField: '',
            formatter: (rowContent, row) =>{
                return(
                    <div>
                        <button onClick={handleDeletePatients.bind(this, row)}>Delete</button>
                        <button onClick={executeUpdate.bind(this, row)}>Update</button>
                        <button onClick={handleAssignButton.bind(this, row)}>Assign Caregiver</button>
                        <button onClick={this.toggleForm.bind(this, row)}>Create Medication Plan</button>
                    </div>
                )
            },
            editable: false,
        }]

    render() {
        return(
            <div>
                <Table data={this.state.tableData} columns={this.columns}/>
                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Medication Plan: </ModalHeader>
                    <ModalBody>
                        <MedicationPlanForm patientID={this.state.selectedIDFroMedicationPlan} medicationList={this.state.options}/>
                    </ModalBody>
                </Modal>
            </div>
        )
    }
}

export default PatientTable;