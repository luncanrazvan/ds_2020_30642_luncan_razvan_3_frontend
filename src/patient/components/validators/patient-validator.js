const minLengthValidator = (value, minLength) => {
    return value.length >= minLength;
};

const validate = (value, rules) => {
    let isValid = true;

    for (let rule in rules) {

        switch (rule) {
            case 'minLength': isValid = isValid && minLengthValidator(value, rules[rule]);
                break;

            default: isValid = true;
        }
    }

    return isValid;
};

export default validate;