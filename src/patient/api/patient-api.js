import {HOST} from '../../commons/hosts'
import RestApiClient from "../../commons/api/rest-client"

const endpoint = {
    patient: '/patient'
}

function getPatients(callback) {

    let request = new Request(HOST.backend_api + endpoint.patient, {
        method:'GET',
    });
    console.log(request.url)
    RestApiClient.performRequest(request, callback);
}

function postPatient(patient, callback) {
    let request = new Request(HOST.backend_api + endpoint.patient,{
        method: 'POST',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    });
    RestApiClient.performRequest(request, callback);
    window.location.reload(false);
}

function updatePatient(patient, callback) {
    let request = new Request(HOST.backend_api + endpoint.patient,{
        method: 'PUT',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    });
    RestApiClient.performRequest(request, callback);
    window.location.reload(false);
}

function deletePatients(params, callback) {

    let request = new Request(HOST.backend_api + endpoint.patient + '/' + params, {
        mode:"cors",
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type':'application/json',
            'Access-Control-Allow-Origin':'*',
        },
        body: JSON.stringify(params)
    });
    console.log('URL:' + request.url);
    RestApiClient.performRequest(request, callback);
    window.location.reload(false);
}

function assignCareGiver(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + '/assignCareGiver', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type':'application/json',
        },
        body: JSON.stringify(params)
    });
    console.log('URL:' + request.url);
    RestApiClient.performRequest(request, callback);
    window.location.reload(false);
}

function assignMedicationPlan(params, callback) {
    let request = new Request(HOST.backend_api + '/medicationPlan', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type':'application/json',
        },
        body: JSON.stringify(params)
    });
    console.log('URL:' + request.url);
    RestApiClient.performRequest(request, callback);
    window.location.reload(false);
}

export {
    getPatients,
    postPatient,
    updatePatient,
    deletePatients,
    assignCareGiver,
    assignMedicationPlan,
}