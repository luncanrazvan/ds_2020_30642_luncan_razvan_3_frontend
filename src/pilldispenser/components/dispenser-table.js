import React from "react";
import Table from "../../commons/tables/table";
import JsonRpcClient from "react-jsonrpc-client";
import {HOST} from "../../commons/hosts";

function setMedicationTaken(row) {
    let api = new JsonRpcClient({
        endpoint: HOST.backend_api + '/rpctesting',
        headers: {
            'X-Token': this.state.token,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    })
    api.request(
        'setMedicationPlanAsTaken',
        {"medicationPlanID": row.medicationPlanID},
    ).then(function(response) {
        if(response === "TOO EARLY"){
            alert("Medication plan cannot be taken earlier");
        }
    })
}

class DispenserTable extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            tableData: props.tableData,
        }
    }

    columns = [
        {
            text: 'Medication Plan ID',
            dataField: 'medicationPlanID',
            editable: false
        },
        {
            text: 'Intake Intervals',
            dataField: 'intakeIntervals',
            editable: false
        },
        {
            text: 'Beginning Of Treatment',
            dataField: 'beginningOfTreatment',
            editable: false
        },
        {
            text: 'Ending Of Treatment',
            dataField: 'endingOfTreatment',
            editable: false
        },
        {
            text:'Actions',
            dataField: '',
            editable: false,
            formatter: (rowContent, row) =>{
                return(
                    <div>
                        <button onClick={setMedicationTaken.bind(this, row)}>Take</button>
                    </div>
                )
            }
        }]

    render() {
        return(
            <div>
                <Table data={this.state.tableData} columns={this.columns}/>
            </div>
        )
    }
}

export default DispenserTable;