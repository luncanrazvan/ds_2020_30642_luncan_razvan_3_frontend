import React from "react";
import DispenserTable from "./components/dispenser-table";
import JsonRpcClient from "react-jsonrpc-client";
import {HOST} from "../commons/hosts"

class PillDispenserContainer extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            tableData: [],
            isLoaded: false,
        }
    }

    componentDidMount() {
        this.getMedicationForPatient()
        console.log(this.props.match.params.id)
    }

    getMedicationForPatient() {
        let api = new JsonRpcClient({
            endpoint: HOST.backend_api+'/rpctesting',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("Authorization")}`,
                'X-Token': undefined,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        })
        api.request(
            'getMedicationPlansForPatients',
            {"patientID": this.props.match.params.id},
        ).then(function(response) {
            let tableData = [];
            console.log(response)
            for(let i = 0; i < response.length; i++){
                tableData.push(response[i].result);
            }
            this.setState({tableData: tableData,
                isLoaded: true})
        }.bind(this))
    }

    render() {
        return(
            <div>
                {this.state.isLoaded && <DispenserTable tableData={this.state.tableData}/>}
            </div>
        )
    }
}

export default PillDispenserContainer;