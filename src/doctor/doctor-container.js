import React from "react"
import {Button} from "reactstrap";

function onManagePatientsButtonPressed() {
    localStorage.setItem('patient', 'true')
    window.location.href = '/patient';
}

function onManageCareGiversButtonPressed() {
    localStorage.setItem('caregiver', 'true');
    window.location.href = '/caregiver';
}

function onManageMedicationButtonPressed() {
    localStorage.setItem('medication', 'true');
    window.location.href = '/medication'
}

function onManagePillDispenserButtonPressed() {
    //localStorage.setItem('medication', 'true');
    window.location.href = '/notificationMedicationPlan'
}

class DoctorContainer extends React.Component{

    componentDidMount() {
        localStorage.setItem('patient', '');
        localStorage.setItem('caregiver', '');
        localStorage.setItem('medication', '');
    }

    render() {
        return (
            <div style={{position: 'absolute', left: '50%', top: '50%', transform: 'translate(-50%, -50%)'}}>
                <Button onClick={onManagePatientsButtonPressed.bind(this)}>Manage Patients</Button>&nbsp;
                <Button onClick={onManageCareGiversButtonPressed.bind(this)}>Manage Caregivers</Button>&nbsp;
                <Button onClick={onManageMedicationButtonPressed.bind(this)}>Manage Medication</Button>&nbsp;
                <Button onClick={onManagePillDispenserButtonPressed.bind(this)}>Medication Plan Notifications</Button>
            </div>
        )
    }
}

export default DoctorContainer;