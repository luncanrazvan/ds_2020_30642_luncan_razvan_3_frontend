import React from "react";
import Table from "../../commons/tables/table";

class NotificationMedicationPlansTable extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            tableData: props.tableData,
        }
    }

    columns = [
        {
            text: 'Patient ID',
            dataField: 'patientID',
            editable: false
        },
        {
            text: 'Medication Plan ID',
            dataField: 'medicationPlanID',
            editable: false,
        }]

    render() {
        return(
            <div>
                <Table data={this.state.tableData} columns={this.columns}/>
            </div>
        )
    }
}

export default NotificationMedicationPlansTable;