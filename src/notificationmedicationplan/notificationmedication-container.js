import React from "react";
import NotificationMedicationPlansTable from "./components/notificationmedicationplan-table"
import * as API_USERS from "./api/notificationmedicationplan-api"

class NotificationMedicationPlanContainer extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
        }
    }

    componentDidMount() {
        this.fetchNotifications();
    }

    fetchNotifications(){
        return API_USERS.getNotificationMedicationPlans((result, status, err) =>{
            if(result !== null && status === 200){
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            }else{
                this.setState({
                    errorStatus: status,
                    error: err,
                });
            }
        });
    }

    render() {
        return (
            <div>
                {this.state.isLoaded && <NotificationMedicationPlansTable tableData={this.state.tableData}/>}
            </div>
        );
    }
}

export default NotificationMedicationPlanContainer;
