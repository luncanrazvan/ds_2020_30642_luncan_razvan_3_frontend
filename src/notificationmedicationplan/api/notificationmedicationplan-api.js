import {HOST} from '../../commons/hosts'
import RestApiClient from "../../commons/api/rest-client"

const endpoint = {
    notifications: '/notificationMedicationPlan'
}

function getNotificationMedicationPlans(callback) {

    let request = new Request(HOST.backend_api + endpoint.notifications, {
        method:'GET',
    });
    console.log(request.url)
    RestApiClient.performRequest(request, callback);
}

export {
    getNotificationMedicationPlans,
}