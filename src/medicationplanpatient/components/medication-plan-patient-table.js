import React from "react";
import Table from "../../commons/tables/table";

class MedicationPlanPatientTable extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            tableData: props.tableData,
        };
    }

    columns = [
        {
            text: 'Intake Intervals',
            dataField: 'intakeIntervals',
            editable: false,
        },
        {
            text: 'Beginning Date',
            dataField: 'beginningDate',
            editable: false,
        },
        {
            text: 'Ending Date',
            dataField: 'endingDate',
            editable: false,
        },
        {
            text: 'Medication List',
            dataField: 'medicationSet',
            editable: false,
        }]

    render() {
        return(
            <div>
                <Table data={this.state.tableData} columns={this.columns}/>
            </div>
        )
    }
}

export default MedicationPlanPatientTable;