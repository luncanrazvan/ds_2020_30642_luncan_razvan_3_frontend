import React from "react";
import MedicationPlanPatientTable from "./components/medication-plan-patient-table";
import * as API_MEDICATION_PLAN_PATIENT from "./api/medication-plan-patient-api";
import {Button} from "react-bootstrap";

class MedicationPlanPatientContainer extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
        }
    }

    componentDidMount() {
        this.fetchMedicationPlansFromPatient(this.props.match.params.id)
        localStorage.setItem('medicationPlanPatient', '')
        localStorage.setItem('caregivers patients', '')
        localStorage.setItem('pill dispenser', 'true')
    }


    fetchMedicationPlansFromPatient(patientID){
        return API_MEDICATION_PLAN_PATIENT.getMedicationPlansForPatient(patientID,(result, status, error)=>{
            if(result !== null && status === 200){
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            }else{
                this.setState({
                    errorStatus: status,
                    error: error,
                })
            }
        })
    }

    render() {
        return (
            <div>
                <Button onClick={() =>  window.location.href='/rpctesting/' + this.props.match.params.id}>Pill Dispenser</Button>
                {this.state.isLoaded && <MedicationPlanPatientTable tableData={this.state.tableData}/>}
            </div>
        );
    }
}

export default MedicationPlanPatientContainer;