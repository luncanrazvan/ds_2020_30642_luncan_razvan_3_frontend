import React from "react";
import Table from "../../commons/tables/table";

class CaregiversPatientsTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tableData: props.tableData,
        };
    }

    columns = [
        {
            text: 'ID',
            dataField: 'id',
            editable: false
        },
        {
            text: 'Name',
            dataField: 'name',
            editable: false
        },
        {
            text: 'Birth Day',
            dataField: 'birthDate',
            editable: false
        },
        {
            text: 'Address',
            dataField: 'address',
            editable: false
        },
        {
            text: 'Gender',
            dataField: 'gender',
            editable: false
        },
        {
            text: 'Medical Record',
            dataField: 'medicalRecord',
            editable: false
        }]

    render() {
        return(
            <div>
                <Table data={this.state.tableData} columns={this.columns}/>
            </div>
        )
    }
}

export default CaregiversPatientsTable;