import {HOST} from '../../commons/hosts'
import RestApiClient from "../../commons/api/rest-client"

const endpoint = {
    careGiversPatients: '/caregiver/careGiverPatients'
}

function getCareGiversPatients(caregiverID, callback) {

    let request = new Request(HOST.backend_api + endpoint.careGiversPatients + '/' + caregiverID, {
        headers: {
            'Authorization': `Bearer ${localStorage.getItem("Authorization")}`,
            'Accept':'application/json',
            'Content-Type':'application/json'
        },
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getCareGiversPatients,
}