import React from "react";
import CaregiversPatientsTable from "./components/caregivers-patients-table";
import * as API_USERS from "./api/caregiver-patients-api";
import 'bootstrap/dist/css/bootstrap.min.css';
import {HOST} from "../commons/hosts";
import SockJS from "sockjs-client";
import Stomp from "stompjs";

function connect() {
    if(localStorage.getItem('socket connected') != null){
        const socket = new SockJS(HOST.backend_api+'/websocket');
        const stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {
            console.log('Connected: ' + frame);
            stompClient.send("/app/sendMessage", {}, "");
            stompClient.subscribe('/receiveMessage', function (message) {
                alert("A venit" + message);
            });
        });
    }
}

class CaregiversPatientsContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
        }
    }

    componentDidMount() {
        this.fetchCareGivers(this.props.match.params.id);
        localStorage.setItem('patient', '')
        localStorage.setItem('caregiver', '');
        localStorage.setItem('caregivers patients', '')
        localStorage.setItem('socket connected', 'connected');
        connect();
    }

    fetchCareGivers(caregiverID){
        return API_USERS.getCareGiversPatients(caregiverID, (result, status, err) =>{
            if(result !== null && status === 200){
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            }else{
                this.setState({
                    errorStatus: status,
                    error: err,
                });
            }
            console.log(this.state.tableData);
        });
    }

    render() {
        return (
            <div>
                {this.state.isLoaded && <CaregiversPatientsTable tableData={this.state.tableData}/>}
            </div>
        );
    }
}

export default CaregiversPatientsContainer