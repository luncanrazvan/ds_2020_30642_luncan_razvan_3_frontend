import React from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom'
import ProtectedRoute from "react-protected-route-component";
import './App.css';
import Home from "./home/home"
import CaregiverContainer from "./caregiver/caregiver-container";
import PatientContainer from "./patient/patient-container";
import MedicationContainer from "./medication/medication-container";
import CaregiversPatientsContainer from "./caregiverspatients/caregiver-patients-container";
import MedicationPlanPatientContainer from  "./medicationplanpatient/medication-plan-patient-container";
import DoctorContainer from "./doctor/doctor-container";
import NotificationContainer from "./notifications/notification-container";
import NotificationMedicationPlanContainer from "./notificationmedicationplan/notificationmedication-container";
import PillDispenserContainer from "./pilldispenser/pill-dispenser-container";
import JsonRpcClient from "react-jsonrpc-client";
import {HOST} from "./commons/hosts";

function checkIfMedicationWasTaken() {
    let api = new JsonRpcClient({
        endpoint: HOST.backend_api+'/rpctesting',
        headers: {
            'X-Token': undefined,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    })
    api.request(
        'checkIfMedicationWasTaken',
        {},
    ).then(function(response) {
        console.log(response)
    })
}

function refreshTakenMedicationPlan() {
    let api = new JsonRpcClient({
        endpoint: HOST.backend_api+'/rpctesting',
        headers: {
            'X-Token': undefined,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    })
    api.request(
        'refreshTakenMedicationPlan',
        {},
    ).then(function(response) {
        console.log("Database refreshed")
    })
}

 function countDown(){
     setInterval(() =>{
         let currentDate = new Date();
         let currentHour = String(currentDate.getHours()+":"+
             currentDate.getMinutes()+":"+
             currentDate.getSeconds())

         if(currentHour.toLocaleString().localeCompare("23:59:59") === 0){
             refreshTakenMedicationPlan()
         }
         checkIfMedicationWasTaken()
         console.log(currentHour.toLocaleString());
     }, 1000);
 }

function App() {

    countDown();

    return (
      <Router>
          <div>
            <Route exact path = '/doctor'
                render={() => <DoctorContainer/>}
            />

            <Route exact path = '/'
                 render={() => <Home/>}
            />

            <Route exact path = '/home'
                render={() => <Home/>}
            />

            <Route exact path= '/notifications'
               render={() => <NotificationContainer/>}
            />

            <Route exact path= '/notificationMedicationPlan'
                 render={() => <NotificationMedicationPlanContainer/>}
            />

            <ProtectedRoute
                path='/rpctesting/:id'
                guardFunction={() => {
                    const token = localStorage.getItem('pill dispenser');
                    return !!token;
                }}
                component={(props) => <PillDispenserContainer {...props}/>}
            />

            <ProtectedRoute
              path="/medication"
              redirectRoute="/doctor"
              guardFunction={() => {
                  const token = localStorage.getItem('medication');
                  return !!token;
              }}
              component={MedicationContainer}
              exact
            />

            <ProtectedRoute
                path="/patient"
                redirectRoute="/doctor"
                guardFunction={() => {
                  const token = localStorage.getItem('patient');
                  return !!token;
                }}
                component={PatientContainer}
                exact
            />

            <ProtectedRoute
                path="/caregiver"
                redirectRoute="/doctor"
                guardFunction={() => {
                   const token = localStorage.getItem('caregiver');
                   return !!token;
                }}
                component={CaregiverContainer}
                exact
            />

            <ProtectedRoute
                path="/caregiver/careGiverPatients/:id"
                redirectRoute="/home"
                guardFunction={() => {
                    const token = localStorage.getItem('caregivers patients');
                    return !!token;
                }}
                component={(props) => <CaregiversPatientsContainer {...props}/>}
                exact
            />

            <ProtectedRoute
              path="/patient/medicationPlanPatients/:id"
              redirectRoute="/home"
              guardFunction={() => {
                  const token = localStorage.getItem('medicationPlanPatient');
                  return !!token;
              }}
              component={(props) => <MedicationPlanPatientContainer {...props}/>}
              exact
            />
      </div>
    </Router>
    );
}

export default App;
